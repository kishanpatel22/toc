#include <fstream>
#include <iostream>
#include "src/NFA/NFA.hh" 

using namespace std;

int main(int argc, char const *argv[]) {
    if(argc != 2) {
        cout << "The code usage is ./exe filename" << endl;
        return errno;
    }
    NFA a(argv[1]);
    return 0;
}

