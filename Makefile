# Variables are assigned or initialized by :=
# variables can be dereferenced 

SRC := src
Automata := Automata
NFA := NFA
CFLAGS := -Wall -g -DINLINE
CC := g++ $(CFLAGS)

FLAGS := -std=c++0x -Wall -fsanitize=address -g 

SOURCE := main.cpp								\
		 $(SRC)/$(Automata)/Automata.cpp 		\
		 $(SRC)/$(Automata)/parser.cpp 			\
		 $(SRC)/$(NFA)/NFA.cpp					

OBJECT := $(SOURCE:.cpp=.o)

run : $(OBJECT)
	$(CC) $(FLAGS) $(OBJECT) -o toc
	
# $@ means name of traget it is going to expand to filename of target
# $? means list of all the sources which are out of date
#
# $# all the dependencies

# This is a pattern rule which baically tells
# make is looking for any .o file which depends on the .c file
# of the same (having same stem names) 

%.o : %.cpp 
			$(CC) $(FLAGS) -c $< -o $@

# $< is an automatic variable given by make, it expands to the 
#    name of the first dependencies

# $^ is an automatic variable which expand to name of all the 
#    dependencies.


# clean is basically a phony rule doesn't generate any file
# it instead deletes *.o files in this case

clean: 
	rm *.o
	rm $(SRC)/$(Automata)/*.o
	rm $(SRC)/$(NFA)/*.o

