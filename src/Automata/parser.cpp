#include "parser.hh"

/**
 * @breif   initializes the 2-D function pointer array
 *          to repective state transitions functions
 */

void parser :: init_parser() {

    ptr[0][0] =  NULL;
    ptr[0][1] =  & parser :: START_SBRACE;
    ptr[0][2] =  & parser :: START_EBRACE;
    ptr[0][3] =  & parser :: START_COMMA;
    ptr[0][4] =  & parser :: START_SYMBOL;
    ptr[0][5] =  & parser :: START_SPACE;
    ptr[0][6] =  & parser :: START_NON;
    ptr[0][7] =  & parser :: START_ERROR;
    ptr[0][8] =  & parser :: START_STOP;

    ptr[1][0] =  NULL;
    ptr[1][1] =  NULL;
    ptr[1][2] =  & parser :: SBRACE_EBRACE;
    ptr[1][3] =  NULL;
    ptr[1][4] =  NULL;
    ptr[1][5] =  NULL;
    ptr[1][6] =  NULL;
    ptr[1][7] =  NULL;
    ptr[1][8] =  NULL;

    ptr[2][0] =  NULL;
    ptr[2][1] =  & parser :: EBRACE_SBRACE;
    ptr[2][2] =  & parser :: EBRACE_EBRACE;
    ptr[2][3] =  & parser :: EBRACE_COMMA;
    ptr[2][4] =  & parser :: EBRACE_SYMBOL;
    ptr[2][5] =  & parser :: EBRACE_SPACE;
    ptr[2][6] =  & parser :: EBRACE_NON;
    ptr[2][7] =  & parser :: EBRACE_ERROR;
    ptr[2][8] =  & parser :: EBRACE_STOP;

    ptr[3][0] =  NULL;
    ptr[3][1] =  & parser :: COMMA_SBRACE;
    ptr[3][2] =  & parser :: COMMA_EBRACE;
    ptr[3][3] =  & parser :: COMMA_COMMA;
    ptr[3][4] =  & parser :: COMMA_SYMBOL;
    ptr[3][5] =  & parser :: COMMA_SPACE;
    ptr[3][6] =  & parser :: COMMA_NON;
    ptr[3][7] =  & parser :: COMMA_ERROR;
    ptr[3][8] =  & parser :: COMMA_STOP;

    ptr[4][0] =  NULL;
    ptr[4][1] =  & parser :: SYMBOL_SBRACE;
    ptr[4][2] =  & parser :: SYMBOL_EBRACE;
    ptr[4][3] =  & parser :: SYMBOL_COMMA;
    ptr[4][4] =  & parser :: SYMBOL_SYMBOL;
    ptr[4][5] =  & parser :: SYMBOL_SPACE;
    ptr[4][6] =  & parser :: SYMBOL_NON;
    ptr[4][7] =  & parser :: SYMBOL_ERROR;
    ptr[4][8] =  & parser :: SYMBOL_STOP;

    ptr[5][0] =  NULL;
    ptr[5][1] =  & parser :: SPACE_SBRACE;
    ptr[5][2] =  & parser :: SPACE_EBRACE;
    ptr[5][3] =  & parser :: SPACE_COMMA;
    ptr[5][4] =  & parser :: SPACE_SYMBOL;
    ptr[5][5] =  & parser :: SPACE_SPACE;
    ptr[5][6] =  & parser :: SPACE_NON;
    ptr[5][7] =  & parser :: SPACE_ERROR;
    ptr[5][8] =  & parser :: SPACE_STOP;

    ptr[6][0] =  NULL;
    ptr[6][1] =  & parser :: NON_SBRACE;
    ptr[6][2] =  & parser :: NON_EBRACE;
    ptr[6][3] =  & parser :: NON_COMMA;
    ptr[6][4] =  & parser :: NON_SYMBOL;
    ptr[6][5] =  & parser :: NON_SPACE;
    ptr[6][6] =  & parser :: NON_NON;
    ptr[6][7] =  & parser :: NON_ERROR;
    ptr[6][8] =  & parser :: NON_STOP;
}

/* Functions related to state transitions */

/*
 * START state transition functions
 */

void parser :: START_SBRACE() {
    transitions = handle_brackets();
}

void parser :: START_EBRACE() {
    eprintf("expected braket [ "); 
}

void parser :: START_COMMA() {
    eprintf("expected ,"); 
}

void parser :: START_SYMBOL() {
    alpha.push_back(ch);
}

void parser :: START_SPACE() {
    return;
}

void parser :: START_NON() {
    transitions.clear();
}

void parser :: START_ERROR() {
    eprintf("unknown input transition");
}

void parser :: START_STOP() {
    eprintf("NULL GRAPH");
}

/*
 * SBRACE state transition functions
 */

void parser :: SBRACE_EBRACE() {
    return;
}

/*
 * EBRACE state transition functions
 */

void parser :: EBRACE_SBRACE() {
    eprintf("expected ,");
}

void parser :: EBRACE_EBRACE() {
    eprintf("expected braket [ "); 
}

void parser :: EBRACE_COMMA() {
    /* Check the transition and then add */
    state_transitions.push_back(transitions);  
    transitions.clear();
    alpha.clear();
    num_commas += 1;
}

void parser :: EBRACE_SYMBOL() {
    eprintf("expected ,");
}

void parser :: EBRACE_SPACE() {
    /* Check the transition and then add */
    state_transitions.push_back(transitions);  
    transitions.clear();
    alpha.clear();
}

void parser :: EBRACE_NON() {
    eprintf("unexpected behaviour !");
}

void parser :: EBRACE_ERROR() {
    eprintf("unknown input transition");
}

void parser :: EBRACE_STOP() {
    /* Check the transition and then add */
    state_transitions.push_back(transitions);  
    transitions.clear();
    alpha.clear();
}

/*
 * COMMA state transition functions
 */

void parser :: COMMA_SBRACE() {
    transitions = handle_brackets();
}

void parser :: COMMA_EBRACE() {
    eprintf("expected braket [ "); 
}

void parser :: COMMA_COMMA() {
    eprintf("expected ,");
}

void parser :: COMMA_SYMBOL() {
    alpha.push_back(ch);
}

void parser :: COMMA_SPACE() {
    return;
}

void parser :: COMMA_NON() {
    alpha.clear();
    transitions.clear();
}

void parser :: COMMA_ERROR() {
    eprintf("unknown input transition");
}

void parser :: COMMA_STOP() {
    eprintf("expected ,");
}

/*
 * SYMBOL state transition functions
 */

void parser :: SYMBOL_SBRACE() {
    eprintf("unexpected behaviour !");
}

void parser :: SYMBOL_EBRACE() {
    eprintf("expected bracket [");
}

void parser :: SYMBOL_COMMA() {
    CHECK_NUM_STATES(num_commas, state_transitions)
    
    transitions.insert(alpha);
    state_transitions.push_back(transitions);  
    transitions.clear();
    alpha.clear();
    num_commas += 1;
}

void parser :: SYMBOL_SYMBOL() {
    alpha.push_back(ch);
}

void parser :: SYMBOL_SPACE() {
    CHECK_NUM_STATES(num_commas, state_transitions)
    
    transitions.insert(alpha);
    state_transitions.push_back(transitions);  
    transitions.clear();
    alpha.clear();
}

void parser :: SYMBOL_NON() {
    eprintf("Unexpected behaviour !");
}

void parser :: SYMBOL_ERROR() {
    eprintf("unknown input transition");
}

void parser :: SYMBOL_STOP() {
    CHECK_NUM_STATES(num_commas, state_transitions)
    
    transitions.insert(alpha);
    state_transitions.push_back(transitions);  
    transitions.clear();
    alpha.clear();
}

/*
 * SPACE state transition functions
 */

void parser :: SPACE_SBRACE() {
    transitions = handle_brackets();
}

void parser :: SPACE_EBRACE() {
    eprintf("expected bracket [");
}

void parser :: SPACE_COMMA() {
    num_commas += 1;
    return;
}

void parser :: SPACE_SYMBOL() {
    alpha.push_back(ch);
}

void parser :: SPACE_SPACE() {
    return;
}

void parser :: SPACE_NON() {
    transitions.clear();
}

void parser :: SPACE_ERROR() {
    eprintf("unknown input transition");
}

void parser :: SPACE_STOP() {
    return;
}

/*
 * NON state transition functions
 */

void parser :: NON_SBRACE() {
    eprintf("unexpected behaviour !");
}

void parser :: NON_EBRACE() {
    eprintf("unexpected behaviour !");
}

void parser :: NON_COMMA() {
    CHECK_NUM_STATES(num_commas, state_transitions)
    state_transitions.push_back(transitions);  
    transitions.clear();
    alpha.clear();
    num_commas += 1;
}

void parser :: NON_SYMBOL() {
    eprintf("expected ,");
}

void parser :: NON_SPACE() {
    CHECK_NUM_STATES(num_commas, state_transitions)
    
    state_transitions.push_back(transitions);  
    transitions.clear();
    alpha.clear();
}

void parser :: NON_NON() {
    eprintf("Unexpected behaviour !");
}

void parser :: NON_ERROR() {
    eprintf("unknown input transition");
}

void parser :: NON_STOP() {
    CHECK_NUM_STATES(num_commas, state_transitions)
    
    state_transitions.push_back(transitions);  
    transitions.clear();
    alpha.clear();
}

/**
 * @breif     selects a state corresponding to given character
 * @param[in] character
 * @return    integer value representing the state number
 */

int parser :: select_state(char ch) {
    int state;
    if(isalpha(ch) || isdigit(ch) || ch == '*') {
        state = SYMBOL;
    }
    else {
        switch(ch) {
            case ' ':
                state = SPACE;
                break;
            case ',':
                state = COMMA;
                break;
            case '-':
                state = NON;
                break;
            case '[':
                state = SBRACE;
                break;
            case ']':
                state = EBRACE;
                break;
            case '\0':
                state = STOP;
                break;
            default:
                state = ERROR;
                break;
        }
    }
    return state;
}

/**
 * @breif     handles brackets (NFA transitions)
 * @return    set of transition corresponding to 
 *            the state
 */

set<string> parser :: handle_brackets() {
    transitions.clear();
    alpha.clear();
    unsigned int curr_state = SBRACE, next_state, nc;
    index = index + 1;
    nc = 0;
    while(curr_state != EBRACE) {
        switch(curr_state) {
            case SBRACE:
                next_state = select_state(s[index]);
                switch(next_state) {
                    case SBRACE:
                        eprintf("expected ]");
                        break;
                    case EBRACE:
                        transitions.clear();
                        break;
                    case SPACE:
                        break;
                    case SYMBOL:
                        alpha.push_back(s[index]);
                        break;
                    case COMMA:
                        eprintf("expected , ");
                        break;
                    case NON:
                        alpha.clear();
                        break;
                    case STOP:
                        eprintf("expected ]");
                        break;
                    case ERROR:
                        eprintf("error in evalution !");
                        break;
                }
                break;
            case SPACE:
                next_state = select_state(s[index]);
                switch(next_state) {
                    case SBRACE:
                        eprintf("error in evalution !");
                        break;
                    case EBRACE:
                        break;
                    case SPACE:
                        break;
                    case SYMBOL:
                        alpha.push_back(s[index]);
                        break;
                    case COMMA:
                        break;
                    case NON:
                        alpha.clear();
                        break;
                    case STOP:
                        eprintf("expected ]");
                        break;
                    case ERROR:
                        eprintf("error in evalution !");
                        break;
                }
                break;
            case SYMBOL:
                next_state = select_state(s[index]);
                switch(next_state) {
                    case SBRACE:
                        eprintf("error in evalution !");
                        break;
                    case EBRACE:
                        CHECK_NUM_STATES(nc, transitions);
                        transitions.insert(alpha);
                        alpha.clear();
                        break;
                    case SPACE:
                        CHECK_NUM_STATES(nc, transitions);
                        transitions.insert(alpha);
                        alpha.clear();
                        break;
                    case SYMBOL:
                        alpha.push_back(s[index]);
                        break;
                    case COMMA:
                        CHECK_NUM_STATES(nc, transitions);
                        transitions.insert(alpha);
                        alpha.clear();
                        break;
                    case NON:
                        eprintf("error in evalution !");
                        break;
                    case STOP:
                        eprintf("expected ]");
                        break;
                    case ERROR:
                        eprintf("error in evalution !");
                        break;
                }
                break;
            case COMMA:
                next_state = select_state(s[index]);
                nc += 1;
                switch(next_state) {
                    case SBRACE:
                        eprintf("error in evalution !");
                        break;
                    case EBRACE:
                        eprintf("expected ,");
                        break;
                    case SPACE:
                        break;
                    case SYMBOL:
                        alpha.push_back(s[index]);
                        break;
                    case COMMA:
                        eprintf("expected ,");
                        break;
                    case NON:
                        alpha.clear();
                        break;
                    case STOP:
                        eprintf("expected ]");
                        break;
                    case ERROR:
                        eprintf("error in evalution !");
                        break;
                }
                break;
            case NON:
                next_state = select_state(s[index]);
                switch(next_state) {
                    case SBRACE:
                        eprintf("error in evalution !");
                        break;
                    case EBRACE:
                        CHECK_NUM_STATES(nc, transitions);
                        transitions.insert(alpha);
                        alpha.clear();
                        break;
                    case SPACE:
                        CHECK_NUM_STATES(nc, transitions);
                        transitions.insert(alpha);
                        alpha.clear();
                        break;
                    case SYMBOL:
                        eprintf("error in evalution !");
                        break;
                    case COMMA:
                        CHECK_NUM_STATES(nc, transitions);
                        transitions.insert(alpha);
                        alpha.clear();
                        break;
                    case NON:
                        eprintf("errror in evalution !");
                        break;
                    case STOP:
                        eprintf("expected ]");
                        break;
                    case ERROR:
                        eprintf("error in evalution !");
                        break;
                }
                break;
        }
        curr_state = next_state;
        index++;
    }
    index = index - 2;
    return transitions;
}

