#include "Automata.hh"

/**
 * @brief     initializes the Automata variables  
 * @param[in] File name in which automata in present
 * @param[in] set Q
 * @param[in] set sigma
 * @param[in] function delta
 * @param[in] final states F
 * @param[in] number of states
 */

void Automata :: init_automata(
        string name, tuple &automata) {

    char str[MAX];
    ifstream in(name);
    if(!in) {
        cout << "Error in opening the file!" << endl;
    }

    in.getline(str, MAX);
    automata.num_Q = atoi(str);
    init_states(automata.num_Q, 
                automata.Q);

    in.getline(str, MAX);
    init_sigma(str, automata.sigma); 

    for(size_t i = 0; i < automata.num_Q; i++) {
        in.getline(str, MAX);
        cout << str << endl;
        (automata.delta).push_back(tokenize_string(str));
    }

    CHECK_ALL_TRANSITIONS(automata);

    in.getline(str, MAX);
    automata.F = init_F(str, automata.num_Q);
    in.close();
}

/**
 * @breif     initializes the states q(i) of Automata
 * @param[in] number of states
 * @param[in] set Q
 */

void Automata :: init_states(int num_Q, set<string> &Q) {
    int i;
    string q = "q";
    for(i = 0; i < num_Q; i++) {
        Q.insert(q + to_string(i));
    }
} 

/**
 * @breif     initializes the sigma value of Automata
 * @param[in] character array
 * @param[in] set sigma
 */

void Automata :: init_sigma(char str[], set<string> &sigma) {
    char *token;
    token = strtok(str, ",");
    while(token != NULL) {
        string s(token);
        sigma.insert(s);
        token = strtok(NULL, ", ");
    }
}

/**
 * @breif     parser for getting values of state transitions
 * @param[in] character string to be tokenized
 * @return    vector repersenting the particular transition
 *            containing set of transitions on given state
 */

vector<set<string> > Automata :: tokenize_string(char str[]) {
    index = num_commas = 0;
    alpha.clear();
    state_transitions.clear();
    strcpy(s, str);

    int curr_state = START, next_state;
    while(curr_state != STOP) {
        switch(curr_state) {
            case START:
                ch = str[index];
                next_state = select_state(ch);
                break;
            case SBRACE:
                next_state = EBRACE;
                break;
            case EBRACE:
                ch = str[index];
                next_state = select_state(ch);
                break;
            case SPACE:
                ch = str[index];
                next_state = select_state(ch);
                break;
            case SYMBOL:
                ch = str[index];
                next_state = select_state(ch);
                break;
            case COMMA:
                ch = str[index];
                next_state = select_state(ch);
                break;
            case NON:
                ch = str[index];
                next_state = select_state(ch);
                break;
        }
        (this->*ptr[curr_state][next_state])();
        curr_state = next_state;
        index++;
    }
    return state_transitions;
}

/**
 * @breif     initializes the set of final states of Automata
 * @param[in] string containing valid states
 * @return    set of string containg accepted states
 */

set<string > Automata :: init_F(char str[], int num_Q) {
    char *token;
    set<string> temp;
    string q = "q";
    temp.clear();
    token = strtok(str, " ,");
    if(token == NULL) {
        eprintf("Fatal : Automata doesn't have any final states"); 
    }
    temp.insert(q + to_string(atoi(token)));
    while(token != NULL) {
        temp.insert(q + to_string(atoi(token)));
        token = strtok(NULL, " ,");
    }
    return temp;
}

