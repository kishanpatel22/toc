#ifndef AUTOMATA_HH
#define AUTOMATA_HH 1

#include "parser.hh"

/* checks for a valid alphabet input in sigma */

#define CHECK_VALID_ALPHABET(tset, sigma)                               \
            string __temp;                                              \
            for(auto __k = tset.begin(); __k != tset.end(); __k++) {    \
                __temp = *(__k);                                        \
                if((!__temp.empty())                                    \
                        && (sigma.find(__temp) == sigma.end())) {       \
                    eprintf("Fatal : Unknown alphabet transition !");   \
                }                                                       \
            }       

/* checks valid transitions for a given state*/

#define CHECK_STATE_TRANSITIONS(automata, index)                        \
            CHECK_NUM_STATES(automata.num_Q, automata.delta[index])     \
            for(size_t __j = 0; __j < automata.num_Q; __j++) {          \
                CHECK_VALID_ALPHABET((automata.delta[index][__j]),      \
                        (automata.sigma))                               \
            }

/* checks valid transitions for all states */

#define CHECK_ALL_TRANSITIONS(automata)                                 \
            for(size_t __i = 0; __i < automata.num_Q; __i++) {          \
                    CHECK_STATE_TRANSITIONS(automata, __i)              \
            }                                                       

using namespace std;

/**
 *
 * Class to accept and represent a particular
 * Automata and set operations related to it.
 *
 * Automata is inherited from the parser class 
 * since a Automata needs a parser to recognize
 * its state transitions.
 *
 **/

class Automata : public parser {

    public :
        
        /**
         *  Automata is a 5-tuple defined as -
         *  M       =  (Q, sigma, delta, q0, F)
         *  Q       =  set of states
         *  sigma   =  values on which transition occurs
         *  detla   =  function for mapping transition
         *  q0      =  initial state (Assumed to first)
         *  F       =  set of final states
         **/ 
        typedef struct tuple {
            vector<vector<set<string>> > delta;
            unsigned int num_Q;
            set<string> Q, sigma, F;
        } tuple;
        
        /* default constructor - initializes parser 
         * for state transitions.(lexical analyser)
         */
        Automata() {
            init_parser();
        }

        /* initializes an Automata */
        void init_automata(string name, tuple &automata);
        
        /* initializes the number of states */
        void init_states(int num_Q, set<string> &Q);
        
        /* initializes the state transition table */
        void init_sigma(char str[], set<string> &sigma);
        
        /* string tokenizer */
        vector<set<string> >tokenize_string(char str[]);

        /* initializes the set of final states */
        set<string > init_F(char str[], int num_Q);

        ~Automata() {}
};

#endif

