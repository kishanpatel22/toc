#ifndef PARSER_H
#define PARSER_H 1

#include <bits/stdc++.h>
#include <iostream>
#include <string>
#include <fstream>

#define MAX 128
#define STATE 9

#define eprintf(fmt, ...)                                                   \
    fprintf(stderr, "Traceback (most recent call last): " fmt               \
        "\nfile = %s, line number = %d, in <module> = %s()\n"               \
            ##__VA_ARGS__, __FILE__, __LINE__, __func__);                   \
    exit(1);

#define CHECK_NUM_STATES(ccount, vec)                                       \
            if(ccount != vec.size()) {                                      \
                eprintf("ambiguous number state transitions encountered !");\
            }

using namespace std;

/**
 * Class is used to develop a lexical analyzer using a 
 * finite state machine. 
 * The class provides additional functionalities to its 
 * base class for purpose of recognizing state transitions
 **/

class parser {
    public:
        /* string depicts a transition value */
        string alpha;

        /* set of transition for a given state */
        set<string> transitions;
        
        /* repersents all possible transitions 
         * that a state take 
         */
        vector<set<string> > state_transitions;
        char s[MAX], ch;
        unsigned int index, num_commas;
        
        /* 2-D array of function pointers 
         * function pointed by the pointer 
         * will be state transition functions 
         */
        void (parser :: *ptr[STATE - 2][STATE])();
        
        /* Finite number of states */
        enum state { START, 
            SBRACE, 
            EBRACE,
            COMMA,  
            SYMBOL,  
            SPACE,  
            NON,
            ERROR,  
            STOP };

        parser() {
            alpha.clear();
            state_transitions.clear();
            transitions.clear();
            index = num_commas = 0;
            s[0] = ch = '\0';
        }
        
        /* initializes the parser */
        void init_parser();
        
        /* handle the brackets case for FSM */
        set<string> handle_brackets();

        /* selects a state given an input charachter */
        int select_state(char ch);
    
        void START_SBRACE();
        void START_EBRACE();
        void START_COMMA();
        void START_SYMBOL();
        void START_SPACE();
        void START_NON();
        void START_ERROR();
        void START_STOP();
        
        void SBRACE_EBRACE();

        void EBRACE_SBRACE();
        void EBRACE_EBRACE();
        void EBRACE_COMMA();
        void EBRACE_SYMBOL();
        void EBRACE_SPACE();
        void EBRACE_NON();
        void EBRACE_ERROR();
        void EBRACE_STOP();
        
        void COMMA_SBRACE();
        void COMMA_EBRACE();
        void COMMA_COMMA();
        void COMMA_SYMBOL();
        void COMMA_SPACE();
        void COMMA_NON();
        void COMMA_ERROR();
        void COMMA_STOP();
 
        void SYMBOL_SBRACE();
        void SYMBOL_EBRACE();
        void SYMBOL_COMMA();
        void SYMBOL_SYMBOL();
        void SYMBOL_SPACE();
        void SYMBOL_NON();
        void SYMBOL_ERROR();
        void SYMBOL_STOP();
 
        void SPACE_SBRACE();
        void SPACE_EBRACE();
        void SPACE_COMMA();
        void SPACE_SYMBOL();
        void SPACE_SPACE();
        void SPACE_NON();
        void SPACE_ERROR();
        void SPACE_STOP();
 
        void NON_SBRACE();
        void NON_EBRACE();
        void NON_COMMA();
        void NON_SYMBOL();
        void NON_SPACE();
        void NON_NON();
        void NON_ERROR();
        void NON_STOP();
        
        ~parser() {
            alpha.clear();
            transitions.clear();
            state_transitions.clear();
        }
};

#endif

