#include "NFA.hh"

NFA :: NFA(string name) {
    init_automata(name, automata_nfa); 
}

NFA :: ~ NFA() {
    (automata_nfa.Q).clear();
    (automata_nfa.sigma).clear();
    (automata_nfa.delta).clear();    
    (automata_nfa.F).clear();
}

