#include <bits/stdc++.h>
#include <iostream>
#include <string>
#include "../Automata/Automata.hh"

using namespace std;

/** 
 * Class to define NFA, it is inherited 
 * from its base class Automata.
 **/

class NFA : public Automata {
    private:
        tuple automata_nfa;  
    public:
        // Constructor with argument - filename     
        NFA(string name);
        // Destructor 
        ~NFA();
};

